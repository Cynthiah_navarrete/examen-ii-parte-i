# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.urls import reverse_lazy

from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin

from django.http import JsonResponse, HttpResponse
from django.core import serializers

from .models import Autor, Obra
from .forms import inicio, login, Obra, Autor, Registro_Obra, Registro_Autor

# Create your views here.

class obras_new(LoginRequiredMixin, generic.CreateView):

    template_name = "inv/Registro_Obra.html"
    model = Obra
    context_object_name = "obj"
    form_class = Registro_Obra
    login_url = "galeries:login"
    success_url = reverse_lazy("inv:Obras")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

class obras_list(LoginRequiredMixin, generic.ListView):
    template_name = "inv/Obras.html"
    model = Obras
    context_object_name = "obj"
    login_url = "galeries:login"

class obras_update(LoginRequiredMixin, generic.UpdateView):
    template_name = "inv/Obras.html"
    model = Obras
    context_object_name = "obj"
    form_class = Autores
    login_url = "galeries:login"
    success_url = reverse_lazy("inv: Obras")


    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class autores_new(LoginRequiredMixin, generic.CreateView):

    template_name = "inv/Registro_Autor.html"
    model = Autor
    context_object_name = "obj"
    form_class = Registro_Autor
    login_url = "galeries:login"
    success_url = reverse_lazy("inv:Autores")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

class autores_list(LoginRequiredMixin, generic.ListView):
    template_name = "inv/Autores.html"
    model = Autores
    context_object_name = "obj"
    login_url = "galeries:login"

class autores_update(LoginRequiredMixin, generic.UpdateView):
    template_name = "inv/Autores.html"
    model = Autores
    context_object_name = "obj"
    form_class = Autores
    login_url = "galeries:login"
    success_url = reverse_lazy("inv: Obras")
