# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Obra (models.Model):

    Nombre  = models.CharField(max_length = 100)
    Autor   = models.CharField(max_length = 70)
    Estilo  = models.CharField(max_length = 70)
    Tecnica = models.CharField(max_length = 70)

    def __str__(self):
        return self.Nombre


class Autor (models.Model):

    Nombre  = models.CharField(max_length = 100)
    Nombre_de_Usuario  = models.CharField(max_length = 100)
    Nacionalidad   = models.CharField(max_length = 70)
    Tecnica = models.CharField(max_length = 70)
    Movimiento  = models.CharField(max_length = 70)

    def __str__(self):
        return self.Nombre
