"""exa2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from galery import views

app_name = "Galery"

urlpatterns = [

    url(r'^admin/', admin.site.urls),
    url('', views.index, name = "index_view" ),
    path('inicio/', name="inicio"),
    path('login/', auth_views.Login.as_view(template_name="galeries/login.html"), name="login"),
    path('Registro_Obra/', auth_views.obras_new.as_view(template_name="galeries/Registro_Obra.html"), name="Registrar Obras"),
    path('Registro_Autor/', auth_views.autores_new.as_view(template_name="galeries/Registro_Autor.html"), name="Registrar Autores"),
    path('Autores/', views.autores_list.as_view(template_name="galeries/Autores.html"), name="Autores"),
    path('Obras/', views.obras_list.as_view(template_name="galeries/Obras.html"), name="Obras"),
]
